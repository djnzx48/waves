org 32768
main:
	di

	ld hl, 0
	ld (volume+1), hl

	ld hl, 0
	ld (time+1), hl

	ld hl, 0xC0DE

	ld de, 0

	ld b, 16

	xor a
	ld c, a

	ld ixl, %00101101

	jp mainloop_init

	; ---------------------

	; runtime = 10 * 56 + 51 = 611
pwm_loop:			;    /  37
	xor d			;  4 /  41 [/  97]
	add a, e		;  4 /  45 [/ 101]
	ld d, a			;  4 /  49 [/ 105]

	and 16			;  7 /  56 [/ 112]

	out ($fe), a		; 11 /  67 [ 56]

	jp pwm_delay		; 10 /  77

pwm_delay:
	dec c			;  4 /  81
	jr nz, pwm_loop		; 12 /  93 [taken]

				;611 / 648

	; ---------------------

	xor d			;  4 / 652
	add a, e		;  4 / 656
	ld d, a			;  4 / 660

	and 16			;  7 / 667

	ret c			;  5 / 672

	; ---------------------

	out ($fe), a		; 11 / 683	; OUT 12

	xor d			;  4 / 687
	add a, e		;  4 / 691
	ld d, a			;  4 / 695

	and b			;  4 / 699	; OUT 13 value
	ld c, a			;  4 / 703
	xor d			;  4 / 707

	add a, e		;  4 / 711
	ld d, a			;  4 / 715

	ld a, r			;  9 / 724

	ld a, c			;  4 / 728	; OUT 13 value

	; ---------------------

	out ($fe), a		; 11 / 739	; OUT 13

	ld a, d			;  4 / 743

	and b			;  4 / 747	; OUT 14 value
	ld c, a			;  4 / 751
	xor d			;  4 / 755

	add a, e		;  4 / 759
	ld d, a			;  4 / 763

	and b			;  4 / 767	; OUT 15 value
	ld e, a			;  4 / 771

	ld a, r			;  9 / 780

	ld a, c			;  4 / 784	; OUT 14 value

	out ($fe), a		; 11 / 795 [784] [next 840]

	ld a, e			;  4 / 799
	xor d			;  4 / 803

	; 2 spare T-states!

mainloop_init:
	add hl, hl		; 11 / 816
	jr nc, no_carry		;  7 / 823 [828 taken]

	ld (volume+2), a	; 13 / 836

mainloop:
	ld a, e			;  4 / 840
	out ($fe), a		; 11 / 851 [840] [next 896]

	ld a, l			;  4 / 855
	xor ixl			;  8 / 863
	ld l, a			;  4 / 867

	; OUTs:
	;
	;  0	  0
	;  1	 56
	;  2	112
	;  3	168
	;  4	224
	;  5	280
	;  6	336
	;  7	392
	;  8	448
	;  9	504
	; 10	560
	; 11	616
	; 12	672
	; 13	728
	; 14	784
	; 15	840
	; ---------
	; 16	896

out_loop:

volume:
	ld de, 0		; 10 / 877

	; ---------------------

	ld a, d			;  4 / 881
	add a, e		;  4 / 885
	ld d, a			;  4 / 889

	and 16			;  7 / 896

	; OUT 0
	out ($fe), a		; 11 /  11 [896] [0]

	ld a, r			;  9 /  20
	ld c, 11		;  7 /  27

	jp pwm_loop		; 10 /  37

; ============================================

no_carry:			;    / 828

	nop			;  4 / 832
	ex af, af'		;  4 / 836

	ld a, e			;  4 / 840
	out ($fe), a		; 11 / 851 [840] [next 896]

	ex af, af'		;  4 / 855
	ld (volume+2), a	; 13 / 868

	ld a, 0			;  7 / 875
	ld a, 0			;  7 / 882
	ld a, 0			;  7 / 889

	ld a, 0			;  7 / 896
	out ($fe), a		; 11 /  11 [896] [0]

	ld c, a			;  4 /  15

	; ---------------------

time:
	ld de, 0		; 10 /  25

	inc de			;  6 /  31
	ld (time+1), de		; 20 /  51

	ld a, d			;  4 /  55
	and 15			;  7 /  62

	bit 4, d		;  8 /  70
	jr nz, reverse_wave	;  7 /  77 [82 taken]

	ld de, 0		; 10 /  87
	jp continue_wave	; 10 /  97

reverse_wave:

	sub 15			;  7 /  89
	neg			;  8 /  97

continue_wave:

	inc a			;  4 / 101
	ld (volume+1), a	; 13 / 114

	; ---------------------

	ld a, 41		;  7 / 121

	; total time = 40 * (4 + 12) + (4 + 7) = 651
delay_loop:			;651 / 772
	dec a
	jr nz, delay_loop

	nop			;  4 / 776

	xor a			;  4 / 780
	ld e, a			;  4 / 784

	out ($fe), a		; 11 / 795 [784] [next 840]

	jp extra_delay		; 19 / 805

extra_delay:
	add hl, hl		; 11 / 816
	jr nc, no_carry		;  7 / 823 [828 taken]

	jp mainloop		; 10 / 836

